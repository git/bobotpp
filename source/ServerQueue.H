// ServerQueue.H  -*- C++ -*-
// Copyright (c) 1997, 1998 Etienne BERNARD
// Copyright (c) 2002 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#ifndef SERVERQUEUE_H
#define SERVERQUEUE_H

#include <list>

#include "BotThreading.H"
#include "Queue.H"
#include "String.H"

class ServerQueueItem;
class Socket;

enum {
  QUIT_PRIORITY, USERMODE_PRIORITY, CHANNELMODE_PRIORITY,
  KICK_PRIORITY, PONG_PRIORITY, TOPIC_PRIORITY, PART_PRIORITY,
  NICK_PRIORITY, USERHOST_PRIORITY, WHO_PRIORITY,
  JOIN_PRIORITY, PING_PRIORITY, INVITE_PRIORITY, PRIVMSG_PRIORITY,
  NOTICE_PRIORITY
};

static const int QUIT_PENALTY = 1;
static const int USERMODE_PENALTY = 2;
static const int CHANNELMODE_PENALTY = 2;
static const int KICK_PENALTY = 2;
static const int PONG_PENALTY = 1;
static const int TOPIC_PENALTY = 2;
static const int PART_PENALTY = 1;
static const int JOIN_PENALTY = 1;
static const int USERHOST_PENALTY = 1;
static const int WHO_PENALTY = 4;
static const int WHOIS_PENALTY = 1;
static const int NICK_PENALTY = 1;
static const int PING_PENALTY = 1;
static const int INVITE_PENALTY = 1;
static const int PRIVMSG_PENALTY = 1;
static const int NOTICE_PENALTY = 1;


class ServerQueue : public Queue {

  std::list<ServerQueueItem *> serverQueue;
  int penalty;
  BotMutex queue_mutex;

  static int max_penalty;

public:
  ServerQueue(Socket *, bool);
  ~ServerQueue();
  void addItem(ServerQueueItem *);
  void addLine(String, int, int, int);
  bool flush();

  void sendCTCP(String, String, String);
  void sendCTCPReply(String, String, String);
  void sendChannelMode(String);
  void sendChannelMode(String, String, String);
  void sendInvite(String, String);
  void sendJoin(String, String);
  void sendKick(String, String, String);
  void sendNick(String);
  void sendNotice(String, String);
  void sendPart(String);
  void sendPass(String);
  void sendPing(String);
  void sendPong(String);
  void sendPrivmsg(String, String);
  void sendQuit(String);
  void sendTopic(String, String);
  void sendUser(String, String);
  void sendUserMode(String, String);
  void sendUserhost(String);
  void sendWho(String);
  void sendWhois(String);
};

#endif
