// Connection.H  -*- C++ -*-
// Copyright (c) 1997, 1998 Etienne BERNARD

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef CONNECTION_H
#define CONNECTION_H

#include "Socket.H"
#include "String.H"

class Connection {
public:
  Connection(String, int, String);
  Connection(unsigned long, int);
  
  virtual ~Connection();

  int getFileDescriptor() const;
  
  virtual bool connect() = 0;
  virtual bool handleInput() = 0;

protected:
  Socket socket;
};

#endif
