// BanList.H  -*- C++ -*-
// Copyright (C) 2008 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#ifndef BANLIST_H
#define BANLIST_H

#include <algorithm>
#include <functional>
#include <list>
#include <string>

#include <ctime>

#include "BotThreading.H"
#include "Mask.H"

class BanEntry;

class BanList
{
public:
  typedef std::list<BanEntry *> List;
  typedef std::list<Mask> MatchList;
  typedef List::iterator iterator;

private:
  List storage;
  mutable BotMutex ban_mutex;

  iterator find (const Mask&);

public:
  BanList ();
  ~BanList ();

  bool add (const Mask&, std::time_t = -1);
  bool del (const Mask&);

  MatchList find_matches (const Mask&) const;
  MatchList delete_expired_x ();

  template<typename T>
  void foreach (const T & fun)
  {
    BotLock ban_lock (ban_mutex);
    std::for_each (storage.begin (), storage.end (), fun);
  }
};

#endif
