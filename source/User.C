// User.C  -*- C++ -*-
// Copyright (c) 1997, 1998 Etienne BERNARD
// Copyright (C) 2009 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#include "User.H"

#include "UserListItem.H"
#include "Utils.H"

User::User(String n, String uh, String channel, int mode, UserListItem *uli)
  : mode(mode), floodNum(0),
    userListItem(uli),
    nick(n), userhost(uh),
    userkey("")
{ }

User::User(String n, int mode)
  : mode(mode), floodNum(0),
    userListItem(0), nick(n), userhost("")
{ }

int
User::getLevel() const
{
  if (userListItem && userListItem->identified &&
      (userListItem->expirationDate == -1 ||
       userListItem->expirationDate > time(0)))
    return userListItem->level;
  return 0;
}

int
User::getProt() const
{
  if (userListItem && userListItem->identified &&
      (userListItem->expirationDate == -1 ||
       userListItem->expirationDate > time(0)))
    return userListItem->prot;
  return 0;
}

bool
User::getAop() const
{
  if (userListItem && userListItem->identified &&
      (userListItem->expirationDate == -1 ||
       userListItem->expirationDate < time(0)))
    return userListItem->aop;
  return false;
}

bool
User::operator< (const User &rh) const
{
  return Utils::to_lower (nick) < Utils::to_lower (rh.nick);
}

bool
User::operator== (const User &rh) const
{
  return Utils::to_lower (nick) == Utils::to_lower (rh.nick);
}
