// Person.C  -*- C++ -*-
// Copyright (c) 1998 Etienne BERNARD
// Copyright (c) 2005 Clinton Ebadi

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.

#include "Person.H"

#include "Bot.H"
#include "ServerConnection.H"
#include "Utils.H"

Person::Person(Bot *b, String a)
  : bot(b), address(a)
{ }

Person::Person(Person &p)
  : bot(p.bot), address(p.address)
{ }

Person *
Person::copy()
{
  return new Person(*this);
}

String
Person::getNick() const
{
  return Utils::get_nick (address);
}

String
Person::getAddress() const
{
  return address;
}

Person &
Person::operator=(const String & a)
{
  address = a;
  return *this;
}

void
Person::sendNotice(String message)
{
  bot->serverConnection->queue->sendNotice(getNick(), message);
}
